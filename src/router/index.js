import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/home/Home";
import { Notify, Toast } from "vant";
import store from "../store";

const routes = [
  {
    path: "",
    name: "Home",
    component: Home,
    meta: {
      title: "图书兄弟",
    },
  },
  {
    path: "/about",
    name: "About",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
    meta: {
      title: "关于",
    },
  },
  {
    path: "/category",
    name: "category",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/category/Category"),
    meta: {
      title: "分类",
    },
  },
  {
    path: "/shopcart",
    name: "shopcart",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/shopcart/shopcart"),
    meta: {
      title: "购物车",
      isAuthRequired: true, // 授权才能访问 标志
    },
  },
  {
    path: "/profile",
    name: "profile",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/profile/profile"),
    meta: {
      title: "我的",
      isAuthRequired: true, // 授权才能访问 标志
    },
  },
  {
    path: "/Detail",
    name: "Detail",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Detail/Detail"),
    meta: {
      title: "详情",
    },
  },
  {
    path: "/Register",
    name: "Register",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/profile/Register"),
    meta: {
      title: "注册",
    },
  },
  {
    path: "/login",
    name: "login",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/profile/Login"),
    meta: {
      title: "登录",
    },
  },
  {
    path: "/address",
    name: "Address",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/profile/Address"),
    meta: {
      title: "地址管理",
    },
  },
  {
    path: "/addressedit",
    name: "AddressEdit",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/profile/AddressEdit"),
    meta: {
      title: "地址编辑",
    },
  },
  {
    path: "/createorder",
    name: "CreateOrder",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/order/CreateOrder"),
    meta: {
      title: "订单详情",
    },
  },
  {
    path: "/order",
    name: "Order",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/order/Order"),
    meta: {
      title: "订单详情",
    },
  },
  {
    path: "/orderdetail",
    name: "OrderDetail",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/order/OrderDetail"),
    meta: {
      title: "订单详情",
    },
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});
// 添加路由导航守卫：路由跳转之前
router.beforeEach((to, from, next) => {
  // 如果需要授权，但当前状态为假，跳转到 登录页面
  // 判断原元素中的isAuthRequired 和 user下面的isLogin状态是否为false
  if (to.meta.isAuthRequired && store.state.user.isLogin === false) {
    Notify("您还没有登录，请先登录！");
    // 如果没有登录，跳转到 login
    return next("/login");
  } else {
    next(); // 走到这里，证明已登录，直接放行
  }
  document.title = to.meta.title;
});

export default router;
